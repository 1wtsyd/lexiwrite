﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using FastColoredTextBoxNS;
using Microsoft.Win32;

namespace LexiWrite
{
    public partial class Form1 : Form
    {
        public string filePath = string.Empty;
        public Form1()
        {
            InitializeComponent();
            text.ContextMenuStrip = contextMenuStrip1;
            this.Icon = image.icon_white;
        }

        static bool IsDarkThemeEnabled()
        {
            const string registryKeyPath = @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize";
            const string registryValueName = "AppsUseLightTheme";

            int value = (int)Registry.GetValue(registryKeyPath, registryValueName, null);

            return value == 0;
        }

        private void SaveFile(bool saveAs = false)
        {
            if (saveAs == true)
            {
                using (SaveFileDialog sfd = new SaveFileDialog())
                {
                    sfd.FileName = null;
                    sfd.Filter = "Текст (*.txt)|*.txt";
                    sfd.Title = "Сохранение файла...";

                    if (sfd.ShowDialog() == DialogResult.Cancel)
                        return;

                    File.WriteAllText(sfd.FileName, text.Text);
                    this.Text = $"LexiWrite - {Path.GetFileName(sfd.FileName)}";
                    filePath = sfd.FileName;
                }
            }
            if (filePath == string.Empty)
            {
                using (SaveFileDialog sfd = new SaveFileDialog())
                {
                    sfd.FileName = null;
                    sfd.Filter = "Текст (*.txt)|*.txt";
                    sfd.Title = "Сохранение файла...";

                    if (sfd.ShowDialog() == DialogResult.Cancel)
                        return;

                    File.WriteAllText(sfd.FileName, text.Text);
                    this.Text = $"LexiWrite - {Path.GetFileName(sfd.FileName)}";
                    filePath = sfd.FileName;
                }
            }
            else
            {
                File.WriteAllText(filePath, text.Text);
            }
        }

        private void OpenFile()
        {
            using (OpenFileDialog opd = new OpenFileDialog())
            {
                opd.FileName = null;
                opd.Filter = "Текст (*.txt)|*.txt";
                opd.Title = "Сохранение файла...";

                if (opd.ShowDialog() == DialogResult.Cancel)
                    return;

                text.Text = File.ReadAllText(opd.FileName, Encoding.Default);
                this.Text = $"LexiWrite - {Path.GetFileName(opd.FileName)}";
                filePath = opd.FileName;
            }
        }

        private void NewFile()
        {
            text.Clear();
            filePath = string.Empty;
            this.Text = $"LexiWrite - Безымянный*";
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e) => Environment.Exit(1);

        private void новыйToolStripMenuItem_Click(object sender, EventArgs e) => NewFile();

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e) => OpenFile();

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e) => SaveFile();

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e) => SaveFile(true);

        private void вставитьToolStripMenuItem_Click(object sender, EventArgs e) => text.Paste();

        private void вырезатьToolStripMenuItem_Click(object sender, EventArgs e) => text.Cut();

        private void выделитьВсеToolStripMenuItem_Click(object sender, EventArgs e) => text.SelectAll();

        private void вставитьToolStripMenuItem1_Click(object sender, EventArgs e) => text.Paste();

        private void вырезатьToolStripMenuItem1_Click(object sender, EventArgs e) => text.Cut();

        private void выделитьВсеToolStripMenuItem1_Click(object sender, EventArgs e) => text.SelectAll();

        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e) => text.Copy();

        private void копироватьToolStripMenuItem1_Click(object sender, EventArgs e) => text.Copy();


        private void BlachTheme()
        {
            темнаяТемаToolStripMenuItem.Checked = true;
            Color blackTheme = Color.FromArgb(30, 30, 30);
            Color blackThemeWhite = Color.FromArgb(45, 45, 45);
            Color blackThemeWhite2 = Color.FromArgb(108, 108, 108);
            menuStrip1.BackColor = blackThemeWhite;
            text.BackColor = blackTheme;
            label1.ForeColor = Color.White;
            panel1.BackColor = blackThemeWhite;
            text.IndentBackColor = blackThemeWhite;
            text.ServiceLinesColor = blackThemeWhite2;
            contextMenuStrip1.BackColor = blackTheme;
            menuStrip1.ForeColor = Color.White;
            contextMenuStrip1.ForeColor = Color.White;
            text.ForeColor = Color.White;
            menuStrip1.RenderMode = ToolStripRenderMode.System;
            contextMenuStrip1.RenderMode = ToolStripRenderMode.System;
        }
        private void WhiteTheme()
        {
            темнаяТемаToolStripMenuItem.Checked = false;
            Color whiteTheme = Color.White;
            Color whiteThemeWhite = Color.WhiteSmoke;
            Color whiteThemeWhite2 = Color.Silver;
            menuStrip1.BackColor = whiteThemeWhite;
            text.BackColor = whiteTheme;
            text.IndentBackColor = whiteThemeWhite;
            label1.ForeColor = Color.Black;
            text.ServiceLinesColor = whiteThemeWhite2;
            panel1.BackColor = whiteTheme;
            contextMenuStrip1.BackColor = whiteTheme;
            menuStrip1.ForeColor = Color.Black;
            contextMenuStrip1.ForeColor = Color.Black;
            text.ForeColor = Color.Black;
            menuStrip1.RenderMode = ToolStripRenderMode.ManagerRenderMode;
            contextMenuStrip1.RenderMode = ToolStripRenderMode.ManagerRenderMode;
        }
        private void темнаяТемаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (темнаяТемаToolStripMenuItem.Checked == false)
                BlachTheme();
            else
                WhiteTheme();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (IsDarkThemeEnabled())
            {
                BlachTheme();
            }
        }

        private void обновлениеToolStripMenuItem_Click_1(object sender, EventArgs e) => new Update(темнаяТемаToolStripMenuItem.Checked).ShowDialog();

        private int CountWords()
        {
            string texts = text.Text;
            string[] words = texts.Split(new char[] { ' ', '\t', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            return words.Length;
        }

        private int CountCharacters()
        {
            string texts = text.Text;
            int count = 0;
            foreach (char c in texts)
            {
                if (!char.IsWhiteSpace(c))
                {
                    count++;
                }
            }
            return count;
        }

        private void text_TextChanging(object sender, TextChangingEventArgs e) => label1.Text = $"Слов: {CountWords()} | Символов: {CountCharacters()}";

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e) => new About(темнаяТемаToolStripMenuItem.Checked).ShowDialog();
    }
}
