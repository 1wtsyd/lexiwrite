﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace LexiWrite
{
    public partial class Update : Form
    {
        public Update(bool BlackTheme)
        {
            InitializeComponent();
            if (BlackTheme != false)
            {
                this.BackColor = Color.FromArgb(30, 30, 30);
                label1.ForeColor = Color.White;
                label2.ForeColor = Color.White;
                button1.BackColor = Color.FromArgb(45, 45, 45);
                button1.FlatStyle = FlatStyle.Flat;
                button1.ForeColor = Color.White;
                button1.FlatAppearance.BorderColor = Color.FromArgb(98, 98, 98);
                button2.BackColor = Color.FromArgb(45, 45, 45);
                button2.FlatStyle = FlatStyle.Flat;
                button2.ForeColor = Color.White;
                button2.FlatAppearance.BorderColor = Color.FromArgb(98, 98, 98);
            }

            label1.Text = label1.Text.Replace("$$$", Properties.Resources.__version);
            
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.ShowIcon = false;
            new Thread(loadVersion).Start();
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.ShowInTaskbar = false;
        }

        private void loadVersion()
        {
            string version = new System.Net.WebClient().DownloadString("https://gitlab.com/1wtsyd/lexiwrite/-/raw/main/__version");
            label2.Text = label2.Text.Replace("...", version);
            if (Properties.Resources.__version != version)
                button2.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e) => this.Close();

        private void button2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/1wtsyd/lexiwrite/");
        }
    }
}
